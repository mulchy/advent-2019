use crate::error::AdventError;
use crate::io::read_input;
use itertools::Itertools;

#[derive(PartialEq, Debug, Copy, Clone, PartialOrd)]
struct Point {
  x: f64,
  y: f64,
}

impl Point {
  pub fn angle_to(&self, other: &Point) -> f64 {
    let x = other.x - self.x;
    let y = other.y - self.y;

    y.atan2(x)
  }

  pub fn distance_to(&self, other: &Point) -> f64 {
    let x = other.x - self.x;
    let y = other.y - self.y;

    (x.powi(2) + y.powi(2)).sqrt()
  }
}

fn parse(input: &str) -> Vec<Point> {
  let mut output = vec![];
  for (y, line) in input.lines().enumerate() {
    for (x, c) in line.trim().chars().enumerate() {
      if c == '#' {
        output.push(Point {
          x: x as f64,
          y: y as f64,
        });
      }
    }
  }
  output
}

#[test]
fn test_parse() {
  let input = ".#..#
  .....
  #####
  ....#
  ...##";
  let expected = vec![
    Point { x: 1.0, y: 0.0 },
    Point { x: 4.0, y: 0.0 },
    Point { x: 0.0, y: 2.0 },
    Point { x: 1.0, y: 2.0 },
    Point { x: 2.0, y: 2.0 },
    Point { x: 3.0, y: 2.0 },
    Point { x: 4.0, y: 2.0 },
    Point { x: 4.0, y: 3.0 },
    Point { x: 3.0, y: 4.0 },
    Point { x: 4.0, y: 4.0 },
  ];

  assert_eq!(expected, parse(input));
}

fn count_visible(station: &Point, asteroids: &Vec<Point>) -> usize {
  asteroids
    .into_iter()
    .filter(|asteroid| asteroid != &station)
    .sorted_by(|a, b| {
      station
        .angle_to(a)
        .partial_cmp(&station.angle_to(&b))
        .unwrap()
    })
    .group_by(|asteroid| asteroid.angle_to(station))
    .into_iter()
    .count()
}

// there is no way this should work
fn blast_em(station: &Point, asteriods: &Vec<Point>) -> Vec<Point> {
  let mut groups: Vec<(f64, Vec<Point>)> = asteriods
    .into_iter()
    .filter(|asteroid| asteroid != &station)
    .sorted_by(|a, b| {
      station
        .angle_to(a)
        .partial_cmp(&station.angle_to(&b))
        .unwrap()
    })
    .group_by(|asteroid| asteroid.angle_to(station))
    .into_iter()
    .map(|(theta, points)| {
      let mut sorted = points.cloned().collect::<Vec<Point>>();
      sorted.sort_by(|a, b| {
        station
          .distance_to(a)
          .partial_cmp(&station.distance_to(&b))
          .unwrap()
      });
      (theta, sorted)
    })
    .collect();

  let modulus = groups.len();
  let mut i = 0;
  let mut count = 0;
  let mut output = vec![];
  let max = asteriods.len() - 1;

  // orient the laser up
  for (theta, _) in &groups {
    if (theta - std::f64::consts::FRAC_PI_2).abs() < 0.01 {
      break;
    }

    i += 1;
  }

  loop {
    let blastable = &mut groups[i].1;

    if blastable.is_empty() {
      i = (i + 1) % modulus;
      continue;
    }

    count += 1;
    let blasted = blastable.pop().unwrap();
    output.push(blasted);
    if count == max {
      break;
    }
    i = (i + 1) % modulus;
  }
  output
}

#[test]
fn test_blast_em() {
  let input = ".#..##.###...#######
  ##.############..##.
  .#.######.########.#
  .###.#######.####.#.
  #####.##.#.##.###.##
  ..#####..#.#########
  ####################
  #.####....###.#.#.##
  ##.#################
  #####.##.###..####..
  ..######..##.#######
  ####.##.####...##..#
  .#####..#.######.###
  ##...#.##########...
  #.##########.#######
  .####.#.###.###.#.##
  ....##.##.###..#####
  .#.#.###########.###
  #.#.#.#####.####.###
  ###.##.####.##.#..##";

  let asteroids = parse(input);

  let station = find_best_station(&asteroids).unwrap().1;

  let output = blast_em(&station, &asteroids);

  assert_eq!(Point { x: 8.0, y: 2.0 }, output[199]);
}
#[test]
fn test_count_visible() {
  let input = ".#..#
  .....
  #####
  ....#
  ...##";

  let asteroids = parse(input);
  let stations = asteroids.clone();

  assert_eq!(
    vec![7, 7, 6, 7, 7, 7, 5, 7, 8, 7],
    stations
      .iter()
      .map(|station| count_visible(station, &asteroids))
      .collect::<Vec<usize>>()
  );
}

fn find_best_station(asteroids: &Vec<Point>) -> Option<(usize, Point)> {
  let stations = asteroids.clone();

  stations
    .into_iter()
    .map(|station| (count_visible(&station, &asteroids), station))
    .max_by_key(|pair| pair.0)
}

#[test]
fn test_find_best_station() {
  let input = ".#..#
  .....
  #####
  ....#
  ...##";

  let asteroids = parse(input);

  assert_eq!(8, find_best_station(&asteroids).unwrap().0)
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day10.txt")?;
  let asteroids = parse(&input);

  let (part1, station) = find_best_station(&asteroids)?;
  let part2_point = blast_em(&station, &asteroids)[199];
  let part2 = part2_point.x * 100.0 + part2_point.y;
  Ok(format!("Day 10::\nPart 1: {}\nPart 2: {}\n", part1, part2))
}
