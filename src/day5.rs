use crate::error::AdventError;
use crate::io::read_input;
use crate::log::{debug, trace};
use std::collections::VecDeque;
use std::convert::From;

pub type Output = Vec<i64>;
pub type Memory = Vec<i64>;

pub fn parse_memory(input: &str) -> Result<Memory, AdventError> {
  let numbers: Result<Memory, std::num::ParseIntError> = input
    .trim()
    .split(',')
    .map(|op| op.parse::<i64>())
    .collect();

  Ok(numbers?)
}

#[derive(Debug)]
enum Mode {
  Positional,
  Immediate,
  Relative,
}

impl From<i64> for Mode {
  fn from(i: i64) -> Self {
    match i % 10 {
      0 => Mode::Positional,
      1 => Mode::Immediate,
      2 => Mode::Relative,
      _ => panic!("Invalid parameter mode"),
    }
  }
}

#[derive(Debug)]
enum Instruction {
  Add,
  Mult,
  Input,
  Output,
  JumpIfTrue,
  JumpIfFalse,
  LessThan,
  Equal,
  RelativeOffset,
  Halt,
}

#[derive(Debug)]
struct ParameterModes(Mode, Mode, Mode);

#[derive(Debug)]
pub struct IntCodeVM {
  program_counter: usize,
  memory: Memory,
  input: VecDeque<i64>,
  done: bool,
  relative_base: i64,
}

impl IntCodeVM {
  pub fn new(memory: Memory) -> IntCodeVM {
    let mut real_memory = vec![0; 1_000_000_000];
    for (i, val) in memory.into_iter().enumerate() {
      real_memory[i] = val;
    }
    IntCodeVM {
      program_counter: 0,
      memory: real_memory,
      input: VecDeque::new(),
      done: false,
      relative_base: 0,
    }
  }

  pub fn is_running(&self) -> bool {
    !self.done
  }

  pub fn write_input(&mut self, i: i64) {
    self.input.push_back(i);
  }

  fn get_instruction_and_parameter_modes(&self) -> (Instruction, ParameterModes) {
    let op = self.memory[self.program_counter];
    let opcode = op % 100;
    let param_1_mode = (op / 100) % 10;
    let param_2_mode = (op / 1000) % 10;
    let param_3_mode = (op / 10000) % 10;

    let instruction = match opcode {
      1 => Instruction::Add,
      2 => Instruction::Mult,
      3 => Instruction::Input,
      4 => Instruction::Output,
      5 => Instruction::JumpIfTrue,
      6 => Instruction::JumpIfFalse,
      7 => Instruction::LessThan,
      8 => Instruction::Equal,
      9 => Instruction::RelativeOffset,
      99 => Instruction::Halt,
      _ => panic!("Invalid op code"),
    };

    let output = (
      instruction,
      ParameterModes(
        Mode::from(param_1_mode),
        Mode::from(param_2_mode),
        Mode::from(param_3_mode),
      ),
    );

    debug!(
      "opcode={}, p1_mode={}, p2_mode={}, p3_mode={}",
      opcode, param_1_mode, param_2_mode, param_3_mode
    );
    debug!("parsed={:?}", output);
    output
  }

  fn get_parameter(&self, mode: Mode, offset: usize) -> i64 {
    match mode {
      Mode::Immediate => self.memory[self.program_counter + offset],
      Mode::Positional => self.memory[self.memory[self.program_counter + offset] as usize],
      Mode::Relative => {
        self.memory[(self.memory[self.program_counter + offset] + self.relative_base) as usize]
      }
    }
  }

  fn get_write_address(&self, mode: Mode, offset: usize) -> usize {
    match mode {
      Mode::Immediate => unimplemented!(),
      Mode::Positional => self.memory[self.program_counter + offset] as usize,
      Mode::Relative => (self.memory[self.program_counter + offset] + self.relative_base) as usize,
    }
  }

  pub fn step(&mut self) -> Option<i64> {
    let (instruction, modes) = self.get_instruction_and_parameter_modes();

    match instruction {
      Instruction::Add => {
        let x = self.get_parameter(modes.0, 1);
        let y = self.get_parameter(modes.1, 2);
        let write_address = self.get_write_address(modes.2, 3);

        debug!(
          "ADD:: {} + {}, address={}, pc={}",
          x, y, write_address, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        self.memory[write_address] = x + y;
        self.program_counter += 4;

        None
      }

      Instruction::Mult => {
        let x = self.get_parameter(modes.0, 1);
        let y = self.get_parameter(modes.1, 2);
        let write_address = self.get_write_address(modes.2, 3);
        debug!(
          "MULT:: {} * {}, address={}, pc={}",
          x, y, write_address, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        self.memory[write_address] = x * y;
        self.program_counter += 4;

        None
      }

      Instruction::Input => {
        let write_address = self.get_write_address(modes.0, 1);

        debug!(
          "INPUT:: address={}, pc={}",
          write_address, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        self.memory[write_address] = self.input.pop_front()?;
        self.program_counter += 2;

        None
      }

      Instruction::Output => {
        let value = self.get_parameter(modes.0, 1);

        debug!("OUTPUT:: value={}, pc={}", value, self.program_counter);
        trace!("\tmem={:?}", self.memory);

        self.program_counter += 2;
        Some(value)
      }

      Instruction::JumpIfTrue => {
        let test = self.get_parameter(modes.0, 1);
        let new_program_counter = self.get_parameter(modes.1, 2);
        debug!(
          "JUMP-IF-TRUE:: test={}, jump_address={}, pc={}",
          test, new_program_counter, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        if test != 0 {
          self.program_counter = new_program_counter as usize;
        } else {
          self.program_counter += 3
        }

        None
      }

      Instruction::JumpIfFalse => {
        let test = self.get_parameter(modes.0, 1);
        let new_program_counter = self.get_parameter(modes.1, 2);

        debug!(
          "JUMP-IF-FALSE:: test={}, jump_address={}, pc={}",
          test, new_program_counter, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        if test == 0 {
          self.program_counter = new_program_counter as usize;
        } else {
          self.program_counter += 3
        }

        None
      }

      Instruction::LessThan => {
        let x = self.get_parameter(modes.0, 1);
        let y = self.get_parameter(modes.1, 2);
        let write_address = self.get_write_address(modes.2, 3);

        debug!(
          "LESS-THAN:: {} < {}, address={}, pc={}",
          x, y, write_address, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        if x < y {
          self.memory[write_address] = 1;
        } else {
          self.memory[write_address] = 0;
        }
        self.program_counter += 4;

        None
      }

      Instruction::Equal => {
        let x = self.get_parameter(modes.0, 1);
        let y = self.get_parameter(modes.1, 2);
        let write_address = self.get_write_address(modes.2, 3);

        debug!(
          "EQUAL:: {} == {}, address={}, pc={}",
          x, y, write_address, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);

        if x == y {
          self.memory[write_address] = 1;
        } else {
          self.memory[write_address] = 0;
        }
        self.program_counter += 4;

        None
      }

      Instruction::RelativeOffset => {
        let adjust = self.get_parameter(modes.0, 1);

        debug!(
          "RelativeOffset:: adjust={}, old_base={}, pc={}",
          adjust, self.relative_base, self.program_counter
        );
        trace!("\tmem={:?}", self.memory);
        self.relative_base += adjust;
        self.program_counter += 2;
        None
      }

      Instruction::Halt => {
        self.done = true;
        None
      }
    }
  }

  pub fn run(mut self) -> Result<Output, AdventError> {
    let mut output = vec![];
    while !self.done {
      if let Some(o) = self.step() {
        output.push(o)
      }
    }

    Ok(output)
  }
}

fn part1() -> Result<Output, AdventError> {
  let input = read_input("input/day5.txt")?;
  let memory = parse_memory(&input)?;
  let mut vm = IntCodeVM::new(memory);
  vm.write_input(1);
  vm.run()
}

#[cfg(test)]
mod tests {
  use super::{AdventError, IntCodeVM};
  use crate::env_logger::fmt::Target;
  use crate::log::LevelFilter;

  #[test]
  fn test_part_2() -> Result<(), AdventError> {
    env_logger::builder()
      .is_test(true)
      .filter_level(LevelFilter::Debug)
      .target(Target::Stdout)
      .try_init()
      .unwrap();

    let mut vm = IntCodeVM::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]);
    vm.write_input(8);
    let mut output = *vm.run()?.last()?;
    assert_eq!(1, output);

    vm = IntCodeVM::new(vec![
      3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9,
    ]);
    vm.write_input(0);
    output = *vm.run()?.last()?;
    assert_eq!(0, output);

    vm = IntCodeVM::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]);
    vm.write_input(1);
    output = *vm.run()?.last()?;
    assert_eq!(0, output);

    vm = IntCodeVM::new(vec![
      3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9,
    ]);
    vm.write_input(1);
    output = *vm.run()?.last()?;
    assert_eq!(1, output);
    Ok(())
  }
}

fn part2() -> Result<Output, AdventError> {
  let input = read_input("input/day5.txt")?;
  let memory = parse_memory(&input)?;
  let mut vm = IntCodeVM::new(memory);
  vm.write_input(5);
  vm.run()
}

pub fn main() -> Result<String, AdventError> {
  let part1 = part1()?
    .iter()
    .map(|i| i.to_string())
    .collect::<Vec<String>>()
    .join("\n\t");

  let part2 = part2()?
    .iter()
    .map(|i| i.to_string())
    .collect::<Vec<String>>()
    .join("\n\t");

  Ok(format!("Day 5::\nPart 1: {}\nPart 2: {}\n", part1, part2))
}
