use crate::error::AdventError;
use crate::io::read_input;
use regex::Regex;
use std::str::FromStr;

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
struct Position {
  x: i32,
  y: i32,
  z: i32,
}

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
struct Velocity {
  x: i32,
  y: i32,
  z: i32,
}

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
struct Moon {
  s: Position,
  v: Velocity,
}

impl Moon {
  pub fn apply_velocity(&mut self) {
    self.s.x += self.v.x;
    self.s.y += self.v.y;
    self.s.z += self.v.z;
  }

  pub fn potential_energy(&self) -> i32 {
    self.s.x.abs() + self.s.y.abs() + self.s.z.abs()
  }

  pub fn kinetic_energy(&self) -> i32 {
    self.v.x.abs() + self.v.y.abs() + self.v.z.abs()
  }
}

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
struct System {
  io: Moon,
  europa: Moon,
  ganymede: Moon,
  callisto: Moon,
}

fn apply_gravity(a: &mut Moon, b: &mut Moon) {
  if a.s.x > b.s.x {
    a.v.x -= 1;
    b.v.x += 1;
  }

  if a.s.x < b.s.x {
    a.v.x += 1;
    b.v.x -= 1;
  }

  if a.s.y > b.s.y {
    a.v.y -= 1;
    b.v.y += 1;
  }

  if a.s.y < b.s.y {
    a.v.y += 1;
    b.v.y -= 1;
  }

  if a.s.z > b.s.z {
    a.v.z -= 1;
    b.v.z += 1;
  }

  if a.s.z < b.s.z {
    a.v.z += 1;
    b.v.z -= 1;
  }
}

impl System {
  pub fn step(&mut self) {
    apply_gravity(&mut self.io, &mut self.europa);
    apply_gravity(&mut self.io, &mut self.ganymede);
    apply_gravity(&mut self.io, &mut self.callisto);
    apply_gravity(&mut self.europa, &mut self.ganymede);
    apply_gravity(&mut self.europa, &mut self.callisto);
    apply_gravity(&mut self.ganymede, &mut self.callisto);

    self.io.apply_velocity();
    self.europa.apply_velocity();
    self.ganymede.apply_velocity();
    self.callisto.apply_velocity();
  }

  pub fn total_energy(&self) -> i32 {
    self.io.potential_energy() * self.io.kinetic_energy()
      + self.europa.potential_energy() * self.europa.kinetic_energy()
      + self.ganymede.potential_energy() * self.ganymede.kinetic_energy()
      + self.callisto.potential_energy() * self.callisto.kinetic_energy()
  }
}

impl FromStr for System {
  type Err = AdventError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    /*<x=-4, y=-9, z=-3>
      <x=-13, y=-11, z=0>
      <x=-17, y=-7, z=15>
      <x=-16, y=4, z=2>
    */
    let lines: Vec<&str> = s.lines().take(4).collect();

    if lines.len() != 4 {
      return Err(AdventError {
        kind: "input".to_string(),
        message: "bad system config".to_string(),
      });
    }

    let io = lines[0].trim();
    let europa = lines[1].trim();
    let ganymede = lines[2].trim();
    let callisto = lines[3].trim();

    let re = Regex::new(r"<x=(.+), y=(.+), z=(.+)>").unwrap();
    let io_pieces = re.captures(io)?;
    if io_pieces.len() != 4 {
      return Err(AdventError {
        kind: "input".to_string(),
        message: "bad system config".to_string(),
      });
    }

    let io = Moon {
      s: Position {
        x: io_pieces.get(1)?.as_str().parse::<i32>()?,
        y: io_pieces.get(2)?.as_str().parse::<i32>()?,
        z: io_pieces.get(3)?.as_str().parse::<i32>()?,
      },
      v: Velocity { x: 0, y: 0, z: 0 },
    };

    let europa_pieces = re.captures(europa)?;
    if europa_pieces.len() != 4 {
      return Err(AdventError {
        kind: "input".to_string(),
        message: "bad system config".to_string(),
      });
    }

    let europa = Moon {
      s: Position {
        x: europa_pieces.get(1)?.as_str().parse::<i32>()?,
        y: europa_pieces.get(2)?.as_str().parse::<i32>()?,
        z: europa_pieces.get(3)?.as_str().parse::<i32>()?,
      },
      v: Velocity { x: 0, y: 0, z: 0 },
    };

    let ganymede_pieces = re.captures(ganymede)?;
    if ganymede_pieces.len() != 4 {
      return Err(AdventError {
        kind: "input".to_string(),
        message: "bad system config".to_string(),
      });
    }

    let ganymede = Moon {
      s: Position {
        x: ganymede_pieces.get(1)?.as_str().parse::<i32>()?,
        y: ganymede_pieces.get(2)?.as_str().parse::<i32>()?,
        z: ganymede_pieces.get(3)?.as_str().parse::<i32>()?,
      },
      v: Velocity { x: 0, y: 0, z: 0 },
    };
    let callisto_pieces = re.captures(callisto)?;
    if callisto_pieces.len() != 4 {
      return Err(AdventError {
        kind: "input".to_string(),
        message: "bad system config".to_string(),
      });
    }

    let callisto = Moon {
      s: Position {
        x: callisto_pieces.get(1)?.as_str().parse::<i32>()?,
        y: callisto_pieces.get(2)?.as_str().parse::<i32>()?,
        z: callisto_pieces.get(3)?.as_str().parse::<i32>()?,
      },
      v: Velocity { x: 0, y: 0, z: 0 },
    };
    Ok(System {
      io,
      europa,
      ganymede,
      callisto,
    })
  }
}

#[test]
fn test_from_string() {
  let system = System::from_str(
    "<x=-4, y=-9, z=-3>
     <x=-13, y=-11, z=0>
     <x=-17, y=-7, z=15>
     <x=-16, y=4, z=2>",
  )
  .unwrap();

  assert_eq!(-4, system.io.s.x);
  assert_eq!(-9, system.io.s.y);
  assert_eq!(-3, system.io.s.z);

  assert_eq!(-13, system.europa.s.x);
  assert_eq!(-11, system.europa.s.y);
  assert_eq!(0, system.europa.s.z);

  assert_eq!(-17, system.ganymede.s.x);
  assert_eq!(-7, system.ganymede.s.y);
  assert_eq!(15, system.ganymede.s.z);

  assert_eq!(-16, system.callisto.s.x);
  assert_eq!(4, system.callisto.s.y);
  assert_eq!(2, system.callisto.s.z);

  assert_eq!(0, system.io.v.x);
  assert_eq!(0, system.io.v.y);
  assert_eq!(0, system.io.v.z);
  assert_eq!(0, system.europa.v.x);
  assert_eq!(0, system.europa.v.y);
  assert_eq!(0, system.europa.v.z);
  assert_eq!(0, system.ganymede.v.x);
  assert_eq!(0, system.ganymede.v.y);
  assert_eq!(0, system.ganymede.v.z);
  assert_eq!(0, system.callisto.v.x);
  assert_eq!(0, system.callisto.v.y);
  assert_eq!(0, system.callisto.v.z);
}

#[test]
fn test_step() {
  let mut system = System::from_str(
    "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>",
  )
  .unwrap();

  for _ in 0..10 {
    system.step();
  }

  assert_eq!(2, system.io.s.x);
  assert_eq!(1, system.io.s.y);
  assert_eq!(-3, system.io.s.z);

  assert_eq!(1, system.europa.s.x);
  assert_eq!(-8, system.europa.s.y);
  assert_eq!(0, system.europa.s.z);

  assert_eq!(3, system.ganymede.s.x);
  assert_eq!(-6, system.ganymede.s.y);
  assert_eq!(1, system.ganymede.s.z);

  assert_eq!(2, system.callisto.s.x);
  assert_eq!(0, system.callisto.s.y);
  assert_eq!(4, system.callisto.s.z);

  assert_eq!(-3, system.io.v.x);
  assert_eq!(-2, system.io.v.y);
  assert_eq!(1, system.io.v.z);

  assert_eq!(-1, system.europa.v.x);
  assert_eq!(1, system.europa.v.y);
  assert_eq!(3, system.europa.v.z);

  assert_eq!(3, system.ganymede.v.x);
  assert_eq!(2, system.ganymede.v.y);
  assert_eq!(-3, system.ganymede.v.z);

  assert_eq!(1, system.callisto.v.x);
  assert_eq!(-1, system.callisto.v.y);
  assert_eq!(-1, system.callisto.v.z);

  assert_eq!(179, system.total_energy())
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day12.txt")?;
  let mut system1 = System::from_str(&input)?;
  for _ in 0..1000 {
    system1.step();
  }
  Ok(format!(
    "Day 12::\nPart 1: {}\nPart 2: {}\n",
    system1.total_energy(),
    part2(&input)?
  ))
}

fn gcd(x: usize, y: usize) -> usize {
  let mut x = x;
  let mut y = y;
  while y != 0 {
    let t = y;
    y = x % y;
    x = t;
  }
  x
}

fn lcm(x: usize, y: usize) -> usize {
  let d = gcd(x, y);
  (x / d) * y
}

fn part2(input: &str) -> Result<usize, AdventError> {
  let mut system = System::from_str(&input)?;
  let mut iterations = 0;
  let mut x_period = 0;
  let mut y_period = 0;
  let mut z_period = 0;

  let goal = system.clone();

  while x_period == 0 || y_period == 0 || z_period == 0 {
    if system.io.s.x == goal.io.s.x
      && system.europa.s.x == goal.europa.s.x
      && system.ganymede.s.x == goal.ganymede.s.x
      && system.callisto.s.x == goal.callisto.s.x
      && system.io.v.x == 0
    {
      x_period = iterations;
    };

    if system.io.s.y == goal.io.s.y
      && system.europa.s.y == goal.europa.s.y
      && system.ganymede.s.y == goal.ganymede.s.y
      && system.callisto.s.y == goal.callisto.s.y
      && system.io.v.y == 0
    {
      y_period = iterations;
    };

    if system.io.s.z == goal.io.s.z
      && system.europa.s.z == goal.europa.s.z
      && system.ganymede.s.z == goal.ganymede.s.z
      && system.callisto.s.z == goal.callisto.s.z
      && system.io.v.z == 0
    {
      z_period = iterations;
    };

    iterations += 1;
    system.step();
  }

  // println!("{},{},{}", x_period, y_period, z_period);

  Ok(lcm(lcm(x_period, y_period), z_period))
}

#[test]
fn test_part2() {
  let input = "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>";
  assert_eq!(2772, part2(input).unwrap());
}
