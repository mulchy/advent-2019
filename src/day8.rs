use crate::error::AdventError;
use crate::io::read_input;

type Layer = Vec<Vec<u32>>;

fn parse_layers(x: usize, y: usize, input: &str) -> Option<Vec<Layer>> {
  input
    .trim()
    .chars()
    .collect::<Vec<_>>()
    .chunks(x * y)
    .map(|layer| {
      layer
        .iter()
        .map(|c| c.to_digit(10))
        .collect::<Option<Vec<u32>>>()
        .map(|o| o.chunks(x).map(|c| c.to_vec()).collect())
    })
    .collect()
}

fn count_pixels(l: &Layer, value: u32) -> usize {
  let rows = l.iter();
  rows
    .map(|row| {
      let pixels = row.iter();
      pixels.filter(|&&p| p == value).count()
    })
    .sum()
}

fn find_layer_with_fewest_zeros(layers: &[Layer]) -> Option<&Layer> {
  layers.iter().min_by_key(|l| count_pixels(l, 0))
}

fn part_1(x: usize, y: usize, input: &str) -> Option<usize> {
  let layers = parse_layers(x, y, input)?;

  let found: &Layer = find_layer_with_fewest_zeros(&layers)?;

  let ones = count_pixels(found, 1);
  let twos = count_pixels(found, 2);

  Some(ones * twos)
}

fn decode(x: usize, y: usize, input: &str) -> Option<Vec<Vec<usize>>> {
  let layers = parse_layers(x, y, input)?;

  let mut image = vec![vec![2; x]; y];

  for layer in layers {
    for (x, row) in layer.iter().enumerate() {
      for (y, pixel) in row.iter().enumerate() {
        let current_pixel = image[x][y];
        if current_pixel == 2 {
          if *pixel == 0 {
            image[x][y] = 0;
          }

          if *pixel == 1 {
            image[x][y] = 1;
          }
        }
      }
    }
  }
  Some(image)
}

fn print(image: Vec<Vec<usize>>) -> String {
  let mut output: Vec<String> = vec![];
  let black = '🎄';
  let white = '🎅';
  let empty = '🎁';
  let uh_oh = '💣';

  for row in image {
    let mut output_row = vec![];
    for pixel in row {
      let output_pixel = match pixel {
        0 => black,
        1 => white,
        2 => empty,
        _ => uh_oh,
      };

      output_row.push(output_pixel);
    }
    output.push(output_row.into_iter().collect());
  }
  output.join("\n")
}

#[test]
fn test_decode() {
  let image = decode(2, 2, "0222112222120000").unwrap();
  assert_eq!(vec![vec![0, 1], vec![1, 0]], image);
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day8.txt")?;
  let one = part_1(25, 6, &input)?;

  let two = print(decode(25, 6, &input)?);
  Ok(format!("Day 8::\nPart 1: {}\nPart 2:\n{}\n", one, two))
}

#[test]
fn test_part_1() {
  assert_eq!(1, part_1(3, 2, "123456789012").unwrap());
}

#[test]
fn test_find_layer_with_fewest_zeros() {
  let layers = parse_layers(3, 2, "123456789012").unwrap();
  let expected: Layer = vec![vec![1, 2, 3], vec![4, 5, 6]];
  assert_eq!(expected, *find_layer_with_fewest_zeros(&layers).unwrap())
}

#[test]
fn test_count_pixels() {
  let layers = parse_layers(3, 2, "123456789012").unwrap();
  let counts: Vec<usize> = layers.iter().map(|l| count_pixels(l, 0)).collect();
  assert_eq!(vec![0, 1], counts);

  let layer = vec![vec![1, 2, 3], vec![4, 5, 6]];
  assert_eq!(1, count_pixels(&layer, 1));
  assert_eq!(1, count_pixels(&layer, 2));
}

#[test]
fn test_parse_layers() {
  let layers = parse_layers(3, 2, "123456789012").unwrap();
  assert_eq!(
    vec![
      vec![vec![1, 2, 3], vec![4, 5, 6]],
      vec![vec![7, 8, 9], vec![0, 1, 2]]
    ],
    layers
  );
}
