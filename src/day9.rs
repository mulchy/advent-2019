use crate::day5::{parse_memory, IntCodeVM};
use crate::error::AdventError;
use crate::io::read_input;

#[test]
fn test_relative_mode() -> Result<(), AdventError> {
  // use crate::env_logger::fmt::Target;
  // use crate::log::LevelFilter;

  // env_logger::builder()
  //   .is_test(true)
  //   .filter_level(LevelFilter::Debug)
  //   .target(Target::Stdout)
  //   .try_init()
  //   .unwrap();

  let input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
  let mut memory = parse_memory(input)?;

  let mut vm = IntCodeVM::new(memory);

  println!("About to run");
  let mut output = vm.run()?;

  assert_eq!(
    vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
    output
  );

  let input = "1102,34915192,34915192,7,4,7,99,0";
  memory = parse_memory(input)?;
  vm = IntCodeVM::new(memory);
  output = vm.run()?;
  assert!(output[0] >= 999999999999999);

  let input = "104,1125899906842624,99";
  memory = parse_memory(input)?;
  vm = IntCodeVM::new(memory);
  output = vm.run()?;
  assert_eq!(1125899906842624, output[0]);

  Ok(())
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day9.txt")?;
  let memory = parse_memory(&input)?;
  let memory2 = memory.clone();

  let mut vm = IntCodeVM::new(memory);
  vm.write_input(1);
  let part1 = vm.run()?;

  vm = IntCodeVM::new(memory2);
  vm.write_input(2);
  let part2 = vm.run()?;

  Ok(format!(
    "Day 9::\nPart 1: {:?}\nPart 2:: {:?}\n",
    part1[0], part2[0]
  ))
}
