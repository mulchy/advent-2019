use std::collections::HashMap;

use crate::error::AdventError;
use crate::io::read_input;
use std::str::FromStr;

struct Orbit {
  parent: String,
  child: String,
}

impl FromStr for Orbit {
  type Err = AdventError;

  fn from_str(s: &str) -> Result<Self, AdventError> {
    let parts: Vec<&str> = s.split(")").collect();
    Ok(Orbit {
      parent: parts.get(0)?.to_string(),
      child: parts.get(1)?.to_string(),
    })
  }
}

fn parse(input: &str) -> Result<Vec<Orbit>, AdventError> {
  input.lines().map(|l| Orbit::from_str(l.trim())).collect()
}

fn build_graph(input: &[Orbit]) -> HashMap<String, Vec<String>> {
  let mut output = HashMap::new();
  for orbit in input {
    let empty = vec![];
    let children = output.entry(orbit.parent.clone()).or_insert(empty);
    children.push(orbit.child.clone());
  }

  output
}

#[test]
fn test_build_graph() -> Result<(), AdventError> {
  let mut expected = HashMap::new();

  let com_children = vec!["B".to_string()];
  expected.insert("COM".to_string(), com_children);

  let b_children = vec!["C".to_string(), "G".to_string()];
  expected.insert("B".to_string(), b_children);

  let g_children = vec!["H".to_string()];
  expected.insert("G".to_string(), g_children);

  let c_children = vec!["D".to_string()];
  expected.insert("C".to_string(), c_children);

  let d_children = vec!["E".to_string(), "I".to_string()];
  expected.insert("D".to_string(), d_children);

  let e_children = vec!["F".to_string(), "J".to_string()];
  expected.insert("E".to_string(), e_children);

  let j_children = vec!["K".to_string()];
  expected.insert("J".to_string(), j_children);

  let k_children = vec!["L".to_string()];
  expected.insert("K".to_string(), k_children);

  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let result = build_graph(&parse(input)?);

  assert_eq!(expected, result);

  Ok(())
}

fn count_orbits(graph: &HashMap<String, Vec<String>>, start: String, level: usize) -> usize {
  if let Some(children) = graph.get(&start) {
    level
      + children
        .iter()
        .map(|child| count_orbits(&graph, child.to_string(), level + 1))
        .sum::<usize>()
  } else {
    level
  }
}

#[test]
fn test_count_orbits() -> Result<(), AdventError> {
  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let graph = build_graph(&parse(input)?);

  assert_eq!(42, count_orbits(&graph, "COM".to_string(), 0));

  Ok(())
}

fn parent(graph: &HashMap<String, Vec<String>>, node: &str) -> Option<String> {
  graph
    .keys()
    .filter(|key| graph.get(*key).unwrap().contains(&node.to_string()))
    .nth(0)
    .map(|s| s.to_string())
}

fn path_to_root(graph: &HashMap<String, Vec<String>>, node: &str) -> Vec<String> {
  let mut path = vec![node.to_string()];
  if let Some(p) = parent(&graph, node) {
    path.extend(path_to_root(graph, &p));
    path
  } else {
    path
  }
}

#[test]
fn test_path_to_root() -> Result<(), AdventError> {
  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let graph = build_graph(&parse(input)?);

  assert_eq!(
    vec!["K", "J", "E", "D", "C", "B", "COM"],
    path_to_root(&graph, "K")
  );

  Ok(())
}

fn find_common_ancestor(graph: &HashMap<String, Vec<String>>, x: &str, y: &str) -> Option<String> {
  for node in path_to_root(graph, x) {
    for other_node in path_to_root(graph, y) {
      if node == other_node {
        return Some(node);
      }
    }
  }
  None
}

#[test]
fn test_find_common_ancestor() -> Result<(), AdventError> {
  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let graph = build_graph(&parse(input)?);

  assert_eq!("D", find_common_ancestor(&graph, "K", "I")?);

  Ok(())
}

fn path_to_ancestor(graph: &HashMap<String, Vec<String>>, start: &str, goal: &str) -> Vec<String> {
  let mut path = vec![start.to_string()];

  if start == goal {
    return path;
  }

  if let Some(p) = parent(&graph, start) {
    path.extend(path_to_ancestor(graph, &p, goal));
    path
  } else {
    path
  }
}

#[test]
fn test_path_to_ancestor() -> Result<(), AdventError> {
  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let graph = build_graph(&parse(input)?);

  assert_eq!(vec!["K", "J", "E", "D"], path_to_ancestor(&graph, "K", "D"));
  assert_eq!(vec!["I", "D"], path_to_ancestor(&graph, "I", "D"));

  Ok(())
}

fn distance_between_nodes(graph: &HashMap<String, Vec<String>>, x: &str, y: &str) -> Option<usize> {
  if let Some(ancestor) = find_common_ancestor(graph, x, y) {
    return Some(
      (path_to_ancestor(graph, x, &ancestor).len() - 1)
        + (path_to_ancestor(graph, y, &ancestor).len() - 1),
    );
  }

  None
}

#[test]
fn test_distance_between_nodes() -> Result<(), AdventError> {
  let input = "COM)B
  B)C
  C)D
  D)E
  E)F
  B)G
  G)H
  D)I
  E)J
  J)K
  K)L";

  let graph = build_graph(&parse(input)?);

  assert_eq!(4, distance_between_nodes(&graph, "K", "I")?);

  Ok(())
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day6.txt")?;
  let orbits = parse(&input)?;
  let graph = build_graph(&orbits);
  let count = count_orbits(&graph, "COM".to_string(), 0);

  let x = parent(&graph, "YOU")?;
  let y = parent(&graph, "SAN")?;

  let distance = distance_between_nodes(&graph, &x, &y)?;

  Ok(format!(
    "Day 6::\nPart 1: {}\nPart 2: {}\n",
    count, distance
  ))
}
