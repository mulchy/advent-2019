use crate::error::AdventError;
use std::fs::File;
use std::io::prelude::*;

pub fn read_input(pathname: &str) -> Result<String, AdventError> {
  let mut f = File::open(pathname)?;
  let mut contents = String::new();
  f.read_to_string(&mut contents)?;
  Ok(contents)
}
