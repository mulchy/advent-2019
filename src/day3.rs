use std::fs::File;
use std::io;
use std::io::prelude::*;

use std::collections::HashSet;
use std::iter::FromIterator;
use std::str::FromStr;

use crate::error::AdventError;

fn read_input() -> io::Result<String> {
  let mut f = File::open("input/day3.txt")?;
  let mut contents = String::new();
  f.read_to_string(&mut contents)?;
  Ok(contents)
}

fn parse_lines(input: &str) -> Result<Vec<Line>, AdventError> {
  input.trim().split(',').map(|s| s.parse::<Line>()).collect()
}

#[test]
fn test_parse_lines() -> Result<(), AdventError> {
  let mut expected = vec![];
  expected.push(Line {
    dir: Direction::R,
    length: 8,
  });
  expected.push(Line {
    dir: Direction::U,
    length: 5,
  });
  expected.push(Line {
    dir: Direction::L,
    length: 5,
  });
  expected.push(Line {
    dir: Direction::D,
    length: 3,
  });
  assert_eq!(expected, parse_lines("R8,U5,L5,D3")?);

  Ok(())
}

#[derive(Hash, Eq, PartialEq, Debug, Copy, Clone)]
struct Point {
  x: i32,
  y: i32,
}

#[derive(Hash, Eq, PartialEq, Debug)]
struct Line {
  dir: Direction,
  length: i32,
}

impl Point {
  #[allow(dead_code)]
  fn distance_from_origin(&self) -> i32 {
    self.x.abs() + self.y.abs()
  }

  fn step(&self, dir: &Direction) -> Point {
    match dir {
      Direction::U => Point {
        x: self.x,
        y: self.y + 1,
      },
      Direction::D => Point {
        x: self.x,
        y: self.y - 1,
      },
      Direction::L => Point {
        x: self.x - 1,
        y: self.y,
      },
      Direction::R => Point {
        x: self.x + 1,
        y: self.y,
      },
    }
  }
}

type Wire = Vec<Point>;

fn build_wire(start: Point, lines: Vec<Line>) -> Vec<Point> {
  let mut points = vec![start];
  let mut current = start;
  for line in lines {
    for _ in 0..line.length {
      current = current.step(&line.dir);
      points.push(current);
    }
  }
  points
}

#[test]
fn test_build_wire() -> Result<(), AdventError> {
  let lines = parse_lines("R8,U5,L5,D3")?;
  let mut expected = vec![Point { x: 0, y: 0 }];
  expected.push(Point { x: 1, y: 0 });
  expected.push(Point { x: 2, y: 0 });
  expected.push(Point { x: 3, y: 0 });
  expected.push(Point { x: 4, y: 0 });
  expected.push(Point { x: 5, y: 0 });
  expected.push(Point { x: 6, y: 0 });
  expected.push(Point { x: 7, y: 0 });
  expected.push(Point { x: 8, y: 0 });

  expected.push(Point { x: 8, y: 1 });
  expected.push(Point { x: 8, y: 2 });
  expected.push(Point { x: 8, y: 3 });
  expected.push(Point { x: 8, y: 4 });
  expected.push(Point { x: 8, y: 5 });

  expected.push(Point { x: 7, y: 5 });
  expected.push(Point { x: 6, y: 5 });
  expected.push(Point { x: 5, y: 5 });
  expected.push(Point { x: 4, y: 5 });
  expected.push(Point { x: 3, y: 5 });

  expected.push(Point { x: 3, y: 4 });
  expected.push(Point { x: 3, y: 3 });
  expected.push(Point { x: 3, y: 2 });

  assert_eq!(expected, build_wire(Point { x: 0, y: 0 }, lines));

  Ok(())
}

fn find_intersections(wire1: Wire, wire2: Wire) -> Vec<Point> {
  let points_on_path1: HashSet<Point> = HashSet::from_iter(wire1.into_iter());
  let points_on_path2: HashSet<Point> = HashSet::from_iter(wire2.into_iter());

  let mut intersections: HashSet<&Point> = points_on_path1.intersection(&points_on_path2).collect();
  intersections.remove(&Point { x: 0, y: 0 });
  intersections.into_iter().cloned().collect()
}

fn find_closest_intersection(wire1: Wire, wire2: Wire) -> Option<Point> {
  let mut intersections = find_intersections(wire1, wire2);
  intersections.sort_by_key(|p| p.distance_from_origin());
  intersections.first().cloned()
}

fn count_steps_to_point(wire: &Wire, point: &Point) -> i32 {
  wire.into_iter().take_while(|p| p != &point).count() as i32
}

fn find_closest_intersection_distance_by_steps(wire1: &Wire, wire2: &Wire) -> Option<i32> {
  let intersections = find_intersections(wire1.to_owned(), wire2.to_owned());
  let mut distances = intersections
    .iter()
    .map(|p| count_steps_to_point(&wire1, p) + count_steps_to_point(&wire2, p))
    .collect::<Vec<i32>>();
  distances.sort();
  distances.first().cloned()
}

#[test]
fn test_find_closest_intersection_distance_by_steps() -> Result<(), AdventError> {
  let wire1 = build_wire(
    Point { x: 0, y: 0 },
    parse_lines("R75,D30,R83,U83,L12,D49,R71,U7,L72")?,
  );
  let wire2 = build_wire(
    Point { x: 0, y: 0 },
    parse_lines("U62,R66,U55,R34,D71,R55,D58,R83")?,
  );

  assert_eq!(
    610,
    find_closest_intersection_distance_by_steps(&wire1, &wire2).unwrap()
  );
  Ok(())
}

#[test]
fn test_find_closest_intersection() -> Result<(), AdventError> {
  let lines1 = parse_lines("R8,U5,L5,D3")?;
  let mut wire1 = build_wire(Point { x: 0, y: 0 }, lines1);

  let lines2 = parse_lines("U7,R6,D4,L4")?;
  let mut wire2 = build_wire(Point { x: 0, y: 0 }, lines2);

  assert_eq!(
    Point { x: 3, y: 3 },
    find_closest_intersection(wire1, wire2).unwrap()
  );

  wire1 = build_wire(
    Point { x: 0, y: 0 },
    parse_lines("R75,D30,R83,U83,L12,D49,R71,U7,L72")?,
  );
  wire2 = build_wire(
    Point { x: 0, y: 0 },
    parse_lines("U62,R66,U55,R34,D71,R55,D58,R83")?,
  );
  assert_eq!(
    159,
    find_closest_intersection(wire1, wire2)
      .unwrap()
      .distance_from_origin()
  );

  Ok(())
}

#[derive(Hash, Eq, PartialEq, Debug)]
enum Direction {
  U,
  D,
  L,
  R,
}

impl FromStr for Direction {
  type Err = AdventError;

  fn from_str(string: &str) -> Result<Self, AdventError> {
    match string {
      "R" => Ok(Direction::R),
      "L" => Ok(Direction::L),
      "U" => Ok(Direction::U),
      "D" => Ok(Direction::D),
      other => Err(AdventError {
        kind: String::from("input"),
        message: format!("Invalid Direction: {:?}", other),
      }),
    }
  }
}

impl FromStr for Line {
  type Err = AdventError;
  fn from_str(string: &str) -> Result<Self, AdventError> {
    let mut iter = string.chars();

    let dir = iter
      .next()
      .ok_or(AdventError {
        kind: String::from("input"),
        message: String::from("empty direction"),
      })?
      .to_string()
      .parse::<Direction>()?;
    let len = iter.collect::<String>().parse::<i32>()?;
    Ok(Line {
      dir: dir,
      length: len,
    })
  }
}

#[test]
fn test_parse_direction_from_string() {
  assert_eq!(Direction::R, "R".parse::<Direction>().unwrap());
  assert_eq!(Direction::L, "L".parse::<Direction>().unwrap());
  assert_eq!(Direction::U, "U".parse::<Direction>().unwrap());
  assert_eq!(Direction::D, "D".parse::<Direction>().unwrap());
  assert!("".parse::<Direction>().is_err());
  assert!("hello".parse::<Direction>().is_err());
}

#[test]
fn test_parse_line_from_string() {
  assert_eq!(
    Line {
      dir: Direction::U,
      length: 10
    },
    "U10".parse::<Line>().unwrap()
  );
  assert!("".parse::<Line>().is_err());
  assert!("UU".parse::<Line>().is_err());
}

fn part1() -> Result<i32, AdventError> {
  let input = read_input()?;
  let mut lines = input.lines();
  let line1 = lines.next().unwrap();
  let line2 = lines.next().unwrap();

  let wire1 = build_wire(Point { x: 0, y: 0 }, parse_lines(line1)?);
  let wire2 = build_wire(Point { x: 0, y: 0 }, parse_lines(line2)?);

  Ok(
    find_closest_intersection(wire1, wire2)
      .unwrap()
      .distance_from_origin(),
  )
}

fn part2() -> Result<i32, AdventError> {
  let input = read_input()?;
  let mut lines = input.lines();
  let line1 = lines.next().unwrap();
  let line2 = lines.next().unwrap();

  let wire1 = build_wire(Point { x: 0, y: 0 }, parse_lines(line1)?);
  let wire2 = build_wire(Point { x: 0, y: 0 }, parse_lines(line2)?);

  Ok(find_closest_intersection_distance_by_steps(&wire1, &wire2).unwrap())
}

pub fn main() -> Result<String, AdventError> {
  Ok(format!(
    "Day 3::\nPart 1: {}\nPart 2: {}\n",
    part1()?,
    part2()?
  ))
}
