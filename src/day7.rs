use crate::day5::{parse_memory, IntCodeVM, Memory};
use crate::error::AdventError;
use crate::io::read_input;
use std::ops::Range;

use itertools::Itertools;

fn find_max_phase_setting(
  memory: &Memory,
  range: Range<i64>,
  feedback: bool,
) -> Result<i64, AdventError> {
  if range.end - range.start != 5 {
    return Err(AdventError {
      kind: "input".to_string(),
      message: "Range is not of size 5".to_string(),
    });
  }

  let outputs: Result<Vec<i64>, _> = range
    .permutations(5)
    .collect::<Vec<Vec<i64>>>()
    .into_iter()
    .map(|combo| {
      let a = combo[0];
      let b = combo[1];
      let c = combo[2];
      let d = combo[3];
      let e = combo[4];
      if feedback {
        run_amplifiers_with_feedback(&memory, a, b, c, d, e)
      } else {
        run_amplifiers(&memory, a, b, c, d, e)
      }
    })
    .collect();
  Ok(outputs?.into_iter().max()?)
}

#[test]
fn test_find_max_phase_setting() -> Result<(), AdventError> {
  let input = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0";
  let memory = parse_memory(input)?;
  assert_eq!(54321, find_max_phase_setting(&memory, 0..5, false)?);
  let input2 = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0";
  let memory2 = parse_memory(input2)?;

  assert_eq!(65210, find_max_phase_setting(&memory2, 0..5, false)?);

  // use crate::env_logger::fmt::Target;
  // use crate::log::LevelFilter;
  // env_logger::builder()
  // .is_test(true)
  // .filter_level(LevelFilter::Debug)
  // .target(Target::Stdout)
  // .try_init()
  // .unwrap();

  let input_3 =
    "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5";
  let memory_3 = parse_memory(input_3)?;
  assert_eq!(139629729, find_max_phase_setting(&memory_3, 5..10, true)?);

  Ok(())
}

fn run_amplifiers(
  memory: &Memory,
  a: i64,
  b: i64,
  c: i64,
  d: i64,
  e: i64,
) -> Result<i64, AdventError> {
  let amp_a_memory = memory.clone();
  let amp_b_memory = memory.clone();
  let amp_c_memory = memory.clone();
  let amp_d_memory = memory.clone();
  let amp_e_memory = memory.clone();

  let mut amp_a = IntCodeVM::new(amp_a_memory);
  let mut amp_b = IntCodeVM::new(amp_b_memory);
  let mut amp_c = IntCodeVM::new(amp_c_memory);
  let mut amp_d = IntCodeVM::new(amp_d_memory);
  let mut amp_e = IntCodeVM::new(amp_e_memory);

  amp_a.write_input(a);
  amp_a.write_input(0);

  amp_b.write_input(b);
  amp_c.write_input(c);
  amp_d.write_input(d);
  amp_e.write_input(e);

  let b_input = *amp_a.run()?.last()?;
  amp_b.write_input(b_input);

  let c_input = *amp_b.run()?.last()?;
  amp_c.write_input(c_input);

  let d_input = *amp_c.run()?.last()?;
  amp_d.write_input(d_input);

  let e_input = *amp_d.run()?.last()?;
  amp_e.write_input(e_input);

  Ok(*amp_e.run()?.last()?)
}

#[test]
fn test_run_amplifiers() -> Result<(), AdventError> {
  let input = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0";
  let memory = parse_memory(input)?;
  assert_eq!(43210, run_amplifiers(&memory, 4, 3, 2, 1, 0)?);
  Ok(())
}

impl IntCodeVM {
  pub fn run_until_output(&mut self) -> Option<i64> {
    while self.is_running() {
      if let Some(o) = self.step() {
        return Some(o);
      }
    }
    None
  }
}

fn run_amplifiers_with_feedback(
  memory: &Memory,
  a: i64,
  b: i64,
  c: i64,
  d: i64,
  e: i64,
) -> Result<i64, AdventError> {
  let amp_a_memory = memory.clone();
  let amp_b_memory = memory.clone();
  let amp_c_memory = memory.clone();
  let amp_d_memory = memory.clone();
  let amp_e_memory = memory.clone();

  let mut amp_a = IntCodeVM::new(amp_a_memory);
  let mut amp_b = IntCodeVM::new(amp_b_memory);
  let mut amp_c = IntCodeVM::new(amp_c_memory);
  let mut amp_d = IntCodeVM::new(amp_d_memory);
  let mut amp_e = IntCodeVM::new(amp_e_memory);

  amp_a.write_input(a);
  amp_a.write_input(0);

  amp_b.write_input(b);
  amp_c.write_input(c);
  amp_d.write_input(d);
  amp_e.write_input(e);

  let mut amps = vec![amp_a, amp_b, amp_c, amp_d, amp_e];
  let mut last_output = 0;
  for i in (0..amps.len()).cycle() {
    if let Some(o) = amps[i].run_until_output() {
      last_output = o;
      amps[(i + 1) % 5].write_input(o);
      continue;
    } else {
      break;
    }
  }

  Ok(last_output)
}

#[test]
fn test_run_amplifiers_with_feedback() -> Result<(), AdventError> {
  let input =
    "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5";
  let memory = parse_memory(input)?;
  assert_eq!(
    139629729,
    run_amplifiers_with_feedback(&memory, 9, 8, 7, 6, 5)?
  );
  Ok(())
}

pub fn main() -> Result<String, AdventError> {
  let input = read_input("input/day7.txt")?;
  let memory = parse_memory(&input)?;

  let max_phase_setting_part_1 = find_max_phase_setting(&memory, 0..5, false)?;
  let max_phase_setting_part_2 = find_max_phase_setting(&memory, 5..10, true)?;

  Ok(format!(
    "Day 7::\nPart 1: {}\nPart 2: {}\n",
    max_phase_setting_part_1, max_phase_setting_part_2
  ))
}
