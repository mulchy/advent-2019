use std::collections::HashMap;

fn digits(mut n: i32) -> Vec<usize> {
  let mut d = vec![];

  if n == 0 {
    d.push(0);
    return d;
  }

  if n < 0 {
    n = n * -1;
  }

  loop {
    if n <= 0 {
      d.reverse();
      return d;
    }
    let digit = n % 10;
    d.push(digit as usize);
    n = (n - digit) / 10;
  }
}

#[test]
fn test_digits() {
  assert_eq!(vec![1, 2, 3, 4], digits(1234));
  assert_eq!(vec![0], digits(0));
  assert_eq!(vec![1, 2, 3], digits(-123));
}

fn is_valid(n: &i32) -> bool {
  let d = digits(*n);
  let six_digits = d.len() == 6;
  let adjacent = d
    .iter()
    .take(5)
    .zip(d.iter().skip(1).take(5))
    .any(|(a, b)| a == b);
  let increasing = d
    .iter()
    .fold((0 as usize, true), |(last, increasing), curr| {
      (*curr, increasing && curr >= &last)
    })
    .1;

  six_digits && adjacent && increasing
}

fn is_valid2(n: &i32) -> bool {
  let d = digits(*n);
  let six_digits = d.len() == 6;

  let unique_pair = d
    .iter()
    .take(5)
    .zip(d.iter().skip(1).take(5))
    .filter(|(a, b)| a == b)
    .map(|t| t.0)
    .fold(HashMap::new(), |mut acc, curr| {
      let count = acc.entry(curr).or_insert(0);
      *count += 1;
      acc
    })
    .values()
    .any(|v| v == &1);
  let increasing = d
    .iter()
    .fold((0 as usize, true), |(last, increasing), curr| {
      (*curr, increasing && curr >= &last)
    })
    .1;

  six_digits && unique_pair && increasing
}

#[test]
fn test_is_valid() {
  assert!(is_valid(&111111));
  assert!(!is_valid(&223450));
  assert!(!is_valid(&123789));
  assert!(is_valid(&111123));
  assert!(!is_valid(&135679));
  assert!(!is_valid(&771111));
}

#[test]
fn test_is_valid2() {
  assert!(!is_valid2(&111111));
  assert!(is_valid2(&112233));
  assert!(!is_valid2(&123444));
  assert!(is_valid2(&111122));
  assert!(!is_valid2(&223450));
  assert!(!is_valid2(&123789));
  assert!(!is_valid2(&111123));
  assert!(!is_valid2(&135679));
  assert!(!is_valid2(&771111));
}

fn part1() -> usize {
  let min = 240920;
  let max = 789857;
  (min..max + 1).filter(is_valid).count()
}

fn part2() -> usize {
  // use std::time::Instant;
  // let now = Instant::now();

  let min = 240920;
  let max = 789857;
  let count = (min..max + 1).filter(is_valid2).count();

  // let elapsed = now.elapsed();
  // println!("Seconds: {:.2?}", elapsed);

  count
}

pub fn main() -> String {
  format!("Day 4::\nPart 1: {}\nPart 2: {}\n", part1(), part2())
}
