use std::io;
use std::num;

#[derive(Debug)]
pub struct AdventError {
  pub kind: String,    // type of the error
  pub message: String, // error message
}

impl From<io::Error> for AdventError {
  fn from(e: io::Error) -> Self {
    AdventError {
      kind: String::from("io"),
      message: e.to_string(),
    }
  }
}

impl From<num::ParseIntError> for AdventError {
  fn from(e: num::ParseIntError) -> Self {
    AdventError {
      kind: String::from("parse"),
      message: e.to_string(),
    }
  }
}

impl From<std::option::NoneError> for AdventError {
  fn from(_: std::option::NoneError) -> Self {
    AdventError {
      kind: String::from("none"),
      message: String::from("NPE"),
    }
  }
}
