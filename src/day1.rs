use std::fs::File;
use std::io;
use std::io::prelude::*;

// TODO share between days
fn parse() -> io::Result<String> {
  let mut file = File::open("input/day1.txt")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;

  return Ok(contents);
}

fn part1() -> io::Result<i64> {
  let input = parse()?;
  input
    .lines()
    .map(|s| {
      s.parse::<i64>()
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "Failed to parse mass as int"))
        .and_then(|mass| Ok(mass / 3 - 2))
    })
    .sum()
}

fn calculate_fuel(mass: i64) -> i64 {
  let mut total = 0;
  let mut new_fuel = mass / 3 - 2;
  total = total + new_fuel;
  loop {
    new_fuel = new_fuel / 3 - 2;
    total = total + new_fuel;
    if new_fuel <= 6 {
      return total;
    }
  }
}

fn part2() -> io::Result<i64> {
  let input = parse()?;
  input
    .lines()
    .map(|s| {
      s.parse::<i64>()
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "Failed to parse mass as int"))
        .and_then(|mass| Ok(calculate_fuel(mass)))
    })
    .sum()
}

pub fn main() -> io::Result<String> {
  return Ok(format!(
    "Day 1::\nPart 1: {}\nPart 2: {}\n",
    part1()?,
    part2()?
  ));
}
