use std::fs::File;
use std::io;
use std::io::prelude::*;

fn read_input() -> io::Result<String> {
  let mut f = File::open("input/day2.txt")?;
  let mut contents = String::new();
  f.read_to_string(&mut contents)?;
  Ok(contents)
}

fn parse(input: String) -> io::Result<Vec<usize>> {
  input
    .trim()
    .split(',')
    .map(|op| {
      op.parse::<usize>().map_err(|_| {
        io::Error::new(
          io::ErrorKind::InvalidInput,
          "Failed to parse op code as usize",
        )
      })
    })
    .collect()
}

#[test]
fn test_part_1() -> Result<(), std::io::Error> {
  let mut expected: Vec<usize> = vec![3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50];
  assert_eq!(
    expected,
    part1(parse("1,9,10,3,2,3,11,0,99,30,40,50".to_string())?)?
  );

  expected = vec![2, 0, 0, 0, 99];
  assert_eq!(expected, part1(parse("1,0,0,0,99".to_string())?)?);

  expected = vec![2, 3, 0, 6, 99];
  assert_eq!(expected, part1(parse("2,3,0,3,99".to_string())?)?);

  expected = vec![2, 4, 4, 5, 99, 9801];
  assert_eq!(expected, part1(parse("2,4,4,5,99,0".to_string())?)?);

  expected = vec![30, 1, 1, 4, 2, 5, 6, 0, 99];
  assert_eq!(expected, part1(parse("1,1,1,4,99,5,6,0,99".to_string())?)?);
  Ok(())
}

// lots of unsafe indexing
fn part1(mut code: Vec<usize>) -> io::Result<Vec<usize>> {
  let mut index = 0;
  let mut op;
  let mut arg1;
  let mut arg2;
  let mut address;
  loop {
    op = code[index];

    match op {
      1 => {
        arg1 = code[index + 1];
        arg2 = code[index + 2];
        address = code[index + 3];
        code[address] = code[arg1] + code[arg2];
      }
      2 => {
        arg1 = code[index + 1];
        arg2 = code[index + 2];
        address = code[index + 3];
        code[address] = code[arg1] * code[arg2];
      }
      99 => return Ok(code),
      other => {
        return Err(io::Error::new(
          io::ErrorKind::InvalidInput,
          format!("Illegal op code, {}", other),
        ))
      }
    }
    index += 4;
  }
}

fn part2() -> io::Result<usize> {
  for noun in 0..100 {
    for verb in 0..100 {
      let mut part2_input = parse(read_input()?)?;

      part2_input[1] = noun;
      part2_input[2] = verb;

      if part1(part2_input)?[0] == 19690720 {
        return Ok(100 * noun + verb);
      }
    }
  }
  return Ok(0);
}

pub fn main() -> io::Result<String> {
  let mut part1_input = parse(read_input()?)?;
  part1_input[1] = 12;
  part1_input[2] = 2;
  return Ok(format!(
    "Day 2::\nPart 1: {}\nPart 2: {}\n",
    part1(part1_input)?[0],
    part2()?
  ));
}
