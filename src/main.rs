#![feature(try_trait)]

extern crate env_logger;
extern crate itertools;
extern crate log;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day12;
mod error;
mod io;
use error::AdventError;

fn main() -> Result<(), AdventError> {
  env_logger::init();

  println!("{}", day1::main()?);
  println!("{}", day2::main()?);
  println!("{}", day3::main()?);
  println!("{}", day4::main());
  println!("{}", day5::main()?);
  println!("{}", day6::main()?);
  println!("{}", day7::main()?);
  println!("{}", day8::main()?);
  println!("{}", day9::main()?);
  println!("{}", day10::main()?);
  println!("{}", day12::main()?);

  return Ok(());
}
